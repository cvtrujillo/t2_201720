package model.data_structures;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;


public class DoubleLinkedList <Item> implements Iterable<Item> 
{
	private int tamanio;
	private Node primero;
	private Node ultimo;


	/**
	 * Clase auxiliar
	 * @author Usuario
	 *
	 */
	private class Node 
	{
		private Item elemento;
		private Node next;
	}
	/**
	 * 
	 * @author Usuario
	 *
	 */
	private class ListIterator implements Iterator<Item> {
		private Node current = primero;

		public boolean hasNext()  { return current != null;                     }
		public void remove()      { throw new UnsupportedOperationException();  }

		public Item next() {
			if (!hasNext())
				try {
					throw new Exception("Error");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			Item item = current.elemento;
			current = current.next; 
			return item;
		}
	}

	/**
	 * M�todo constructor
	 */
	 public DoubleLinkedList()
	{
		tamanio=0;
		primero=null;
		ultimo=null;

		
		verificarInvariante();
	}

	 /**
	  * M�todo que retorna el primer elemento de la lista
	  * @return
	  */
	 public Node darPrimerElemento()
	 {
		 return primero;
	 }

	 /**
	  * M�todo que retorna el �ltimo elemento de la lista
	  * @return
	  */
	 public Node darUltimoElemento()
	 {
		 return ultimo;
	 }

	 /**
	  * M�todo que retorna el tama�o de la lista
	  * @return
	  */
	 public int tamanito ()
	 {
		 return tamanio;
	 }

	 /**
	  * M�todo que agrega un elemento, lo voy hacer en forma de cola
	  * @param agregar
	  */
	 public void enqueue(Item agregar)
	 {
		 Node viejpUltimo = ultimo;
		 ultimo = new Node();
		 ultimo.elemento = agregar;
		 ultimo.next = null;
		 if (isEmpty()) primero = ultimo;
		 else           viejpUltimo.next = ultimo;
		 tamanio++;
		 verificarInvariante();
	 }

	 /**
	  * M�todo que retorna y elimina al elemento que fue a�anido recientemente
	  * @return
	  */
	 public Item dequeue()
	 {
		 if(isEmpty())
		 {
			 ultimo = null;  

			 try {
				 throw new Exception("Error, lista vac�a");
			 } catch (Exception e) {
				 // TODO Auto-generated catch block
				 e.printStackTrace();
			 }
		 }
		 Item item = primero.elemento;
		 primero = primero.next;
		 tamanio--;

		 verificarInvariante();

		 return item;
		 // to avoid loitering
	 }
	 /**
	  * M�todo que verificia si a lista est� vac�a
	  * @return
	  */
	 private boolean isEmpty() 
	 {
		 // TODO Auto-generated method stub
		 if(primero==null)
		 {
			 return true;
		 }
		 return false;
	 }

	 @Override
	 public Iterator<Item> iterator() {
		 // TODO Auto-generated method stub
	        return new ListIterator();  

	 }
	 


	 /**
	  * M�todo que verifica la invariante
	  */
	 public void verificarInvariante()
	 {
		 assert tamanio!=0 :"La lista no puede quedar vac�a";
		 assert primero!=null :"El primer elemento de la lista no puede ser nulo";
	 }
	 /**public static void main(String[] args) throws Exception {
	        DoubleLinkedList<String> queue = new DoubleLinkedList<String>();
	        while (!Stdln.isEmpty()) {
	            String item = Stdln.readString();
	            if (!item.equals("-"))
	                queue.enqueue(item);
	            
	           else if (!queue.isEmpty())
	        	   throw new Exception("Error");
	          //      StdOut.print(queue.dequeue() + " ");
	        }
	        //StdOut.println("(" + queue.size() + " left on queue)");
	    }
	    */



}
